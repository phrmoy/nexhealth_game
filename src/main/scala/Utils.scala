/**
  * Created by pmoy on 9/23/16.
  */
object Utils {
  val r = scala.util.Random
  type Move = (Int, Int)
  val NOT_MOVED: Move = (-1, -1)
}
