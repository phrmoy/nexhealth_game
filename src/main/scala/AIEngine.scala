import Utils._

/**
  * Created by pmoy on 9/23/16.
  */
abstract class AIEngine(private val search_depth: Int = 6) extends Player {

  def get_moves(game: Board): List[Move] = {
    val (width_value,height_value) = game.get_board_dimenssions
    val length = width_value * height_value
    val moves = game.get_legal_moves
    val check = length - moves.length
    if (check == 0 || check == 1) List[Move](moves(r.nextInt(moves.length)))
    else moves
  }


  def eval_func(game: Board, maximizing_player_turn: Boolean): Double = {
    if (maximizing_player_turn){
      game.get_legal_moves.length - game.get_opponent_moves.length
    } else {
      game.get_opponent_moves.length - game.get_legal_moves.length
    }
  }

  def utility(game: Board, moves: List[Move], maximizing_player_turn:Boolean): Double = {
    if (moves.isEmpty) if (maximizing_player_turn) -1.0/0.0 else 1.0/1.0
    else eval_func(game, maximizing_player_turn)
  }

  def minval(game: Board, A: Double, B: Double, depth: Int): (Move,Double) = {
    val a = A
    var b = B
    val moves = get_moves(game)
    if (moves.isEmpty || depth==0) (NOT_MOVED, utility(game,moves,maximizing_player_turn=false))
    else {
      var move = NOT_MOVED
      var value = 1.0/0.0
      for (m <- moves) {
        val v = maxval(game.forecast_move(m),a,b,depth-1)._2
        val moveValue = List((move,value),(m,v)).minBy(_._2)
        move = moveValue._1
        value = moveValue._2

        if (value <= a){
          return (move,value)
        } else {
          b = math.min(b,value)
        }

      }
      return (move,value)
    }
  }

  def maxval(game: Board, A: Double, B: Double, depth: Int): (Move,Double) = {
    var a = A
    val b = B
    val moves = get_moves(game)
    if (moves.isEmpty || depth==0) (NOT_MOVED, utility(game,moves,maximizing_player_turn=true))
    else {
      var move = NOT_MOVED
      var value = -1.0/0.0
      for (m <- moves) {
        val v = minval(game.forecast_move(m),a,b,depth-1)._2
        val moveValue = List((move,value),(m,v)).maxBy(_._2)
        move = moveValue._1
        value = moveValue._2

        if (value >= b){
          return (move,value)
        } else {
          a = math.max(a,value)
        }

      }
      return (move,value)
    }
  }

  def alphabeta(game: Board): Move = {
    maxval(game, -1.0/0.0, 1.0/0.0, search_depth)._1
  }

  def move(game: Board): Move = {
    alphabeta(game)
  }

}
