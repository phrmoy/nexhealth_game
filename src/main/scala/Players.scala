/**
  * Created by pmoy on 9/23/16.
  */

import scala.util.{Failure, Success, Try}
import Utils._

abstract class Player {
  val id: String = r.alphanumeric take 20 mkString
  val player_type: String
  def move(game: Board): Move
  def == (that: Player): Boolean = {
    this.id == that.id
  }
}

class RandomPlayer extends Player {
  val player_type: String = "Random"
  def move(game: Board): Move = {
    val legal_moves = game.get_legal_moves
    if (legal_moves.isEmpty) NOT_MOVED
    else legal_moves(r.nextInt(legal_moves.length))
  }
}


class HumanPlayer extends Player {
  val player_type: String = "Human"
  def move(game: Board): Move = {
    val legal_moves = game.get_legal_moves
    if (legal_moves.isEmpty) NOT_MOVED
    else {
      println("Choose a move from:")
      for ((m, i) <- legal_moves.zipWithIndex) print(s"[$i]: $m ")
      println()
      print ("Your selection: ")
      val choices: List[Int] = legal_moves.indices.toList
      var choice = scala.io.StdIn.readLine().trim
      val index: Try[Int] = Try(choice.toInt)
      index match {
        case Success(ind) =>
          if (choices.contains(ind)) legal_moves(ind)
          else {
            println()
            println(ind + " is not a valid choice. Out of bounds.")
            move(game)
          }
        case Failure(_) =>
          println()
          println(choice + " is not a valid choice. Failed Parsing.")
          move(game)
      }
    }
  }
}

class DummyPlayer extends Player {
  val player_type: String = "Dummy"
  def move(game: Board): Move = {
    val legal_moves = game.get_legal_moves
    if (legal_moves.isEmpty) NOT_MOVED
    else legal_moves.head
  }
}

class AIPlayer extends AIEngine {
  val player_type: String = "AI"
}
