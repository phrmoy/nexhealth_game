/**
  * Created by pmoy on 9/23/16.
  */
object GameEngine extends App {
  val rplayer = new AIPlayer
  val hplayer = new HumanPlayer
  val game = new Board(rplayer,hplayer,BoardWidth(7),BoardHeight(7))
  val winner = game.play_isolation()

  val winning_player = if (winner == rplayer) "1" else "2"

  println()
  println(s"Player $winning_player is the winner.")
}
