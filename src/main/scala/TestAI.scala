/**
  * Created by pmoy on 9/23/16.
  */
object TestAI extends App {
  val rplayer = new RandomPlayer
  val hplayer = new AIPlayer

  def run: Double = {
    val game = new Board(rplayer,hplayer,BoardWidth(7),BoardHeight(7))
    val winner = game.play_isolation()
    if (winner == rplayer) 0.0 else 1.0
  }

  val sample = 1000
  val winning_rate = (1 to sample).map(_ => run).sum /sample * 100

  println()
  println(s"AI won $winning_rate% of the time against a Random Player.")
}
