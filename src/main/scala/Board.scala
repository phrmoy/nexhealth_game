/**
  * Created by pmoy on 9/23/16.
  */

import Utils._

object BoardState extends Enumeration {
  type State = Value
  val BLANK, BLOCKED = Value
}

abstract class BoardDimension
case class BoardWidth(x: Int = 7) extends BoardDimension
case class BoardHeight(x: Int = 7) extends BoardDimension

class Board(private val player1: Player, private val player2: Player, private val width: BoardDimension, private val height: BoardDimension) {

  import BoardState._

  private val width_value = width match {
    case BoardWidth(x) => x
    case _ => throw new Error("Wrong dimension passed for board width.")
  }
  private val height_value = height match {
    case BoardHeight(x) => x
    case _ => throw new Error("Wrong dimension passed for board height.")
  }

  private var board_state = for {
    h <- 0 until height_value
  } yield for {
    w <- 0 until width_value
  } yield BLANK

  private var last_player_move = Map(player1.id -> NOT_MOVED, player2.id -> NOT_MOVED)

  private var active_player = player1

  private var inactive_player = player2

  def get_state = board_state

  def get_active_player = active_player

  def get_inactive_player = inactive_player

  def get_board_dimenssions = (width_value,height_value)

  private def apply_move(move: Move): Unit = {
    val (row, col) = move
    last_player_move = last_player_move.updated(active_player.id, move)
    board_state = board_state.updated(row, board_state(row).updated(col, BLOCKED))
    val tmp = active_player
    active_player = inactive_player
    inactive_player = tmp
  }

  def copy: Board = {
    val b = new Board(player1, player2, width, height)
    b.last_player_move = last_player_move
    b.active_player = active_player
    b.inactive_player = inactive_player
    b.board_state = board_state
    b
  }

  def forecast_move(move: Move): Board = {
    val new_board = copy
    new_board.apply_move(move)
    new_board
  }

  def get_first_moves: List[Move] = {
    val moves = for {
      h <- 0 until height_value
      w <- 0 until width_value
      if board_state(h)(w) == BLANK
    } yield (h, w)
    moves.toList
  }

  def move_is_legal(move: Move): Boolean = {
    val (row, col) = move
    0 <= row && row < height_value && 0 <= col && col < width_value && board_state(row)(col) == BLANK
  }

  def get_moves(move: Move): List[Move] = {
    if (move == NOT_MOVED) get_first_moves
    else {
      val (row, col) = move
      val directions = List(
        (-2, -1), (-2, 1),
        (-1, -2), (-1, 2),
        (1, -2), (1, 2),
        (2, -1), (2, 1)
      )
      for {
        (dr, dc) <- directions
        if move_is_legal(row + dr, col + dc)
      } yield (row + dr, col + dc)
    }
  }

  def get_legal_moves: List[Move] = {
    get_moves(last_player_move(active_player.id))
  }

  def get_opponent_moves: List[Move] = {
    get_moves(last_player_move(inactive_player.id))
  }

  def print_board: Unit = {
    val (p1_row, p1_col) = last_player_move(player1.id)
    val (p2_row, p2_col) = last_player_move(player2.id)

    val states = board_state

    var print_out = ""

    for {
      h <- 0 until height_value

    } {
      for {
        w <- 0 until width_value
      } {
        if (states(h)(w) == BLANK) {
          print_out += " "
        } else if (h == p1_row && w == p1_col) {
          print_out += "1"
        } else if (h == p2_row && w == p2_col) {
          print_out += "2"
        } else {
          print_out += "-"
        }
        print_out += " | "
      }
      print_out += "\n\r"
    }
  println(print_out)
  }

  def play_isolation(round: Int = 1): Player = {

    println (s"Round $round")

    if (active_player.player_type=="Human" || active_player.player_type=="Dummy"){
      print_board
      println()
    }

    val move = active_player.move(this)
    if (move == NOT_MOVED) inactive_player
    else {
      apply_move(move)
      play_isolation(round+1)
    }
  }

}
