# Isolation
This is a simple implementation of the AI for the game _Isolation_.

## The game

The rules of Isolation are simple. Two players take turns placing their own game piece on different squares of a 7-by-7 grid. After the first two moves (in which each player puts their piece on any unoccupied square), the pieces move like knights in chess: two squares vertically and a square horizontally, or two squares horizontally and a square vertically (an L-shape). Each time a player moves their piece, the square that they were previously occupying is blocked and cannot be moved to, but can be moved through, for the remainder of the game. In addition, the players may not occupy the same square but they may move through each other. The first player who is unable to move loses.

## Running the game

`bash test_AI.sh`: This will run the AI against the RandomPlayer for 1000 games and display the winning ratio.  
`bash play_AI.sh`: This will initiate a game against the AI.